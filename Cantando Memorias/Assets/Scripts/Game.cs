﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

//using Boo.Lang;

namespace SiriusGames
{
    public class Game : MonoBehaviour
    {
        private Pergunta[] questions;
        private Pergunta[] playQuestions = new Pergunta[GameConstants.QUESTION_MAX];
        [HideInInspector] public int Quest;
        private int stage;

        void Start()
        {
            Quest = 0;

//            Shuffle();
            CurrentQuestion();
        }


        public void Shuffle()
        {
            questions = Resources.LoadAll<Pergunta>("Questions");

            int randomNumber;
            for (int i = 0; i < playQuestions.Length; i++)
            {
                Reshuffle:
                randomNumber = Random.Range(0, questions.Length);
                var newQuestion = questions[randomNumber];

                if (!Array.Exists(playQuestions, element => element == newQuestion))
                {
                    playQuestions[i] = newQuestion;
                }
                else
                {
                    goto Reshuffle;
                }
            }

//            for (int i = 0; i < playQuestions.Length; i++)
//            {
//                Pergunta_som questionSound;
//                if (playQuestions[i] is Pergunta_som)
//                {
//                    questionSound = (Pergunta_som) playQuestions[i];
//                    questionSound.sound.LoadAudioData();
//                }
//            }

//            for (int i = 0; i < questions.Length; i++)
//            {
//                questions[i] = null;
//            }
//            Resources.UnloadUnusedAssets();
        }

        public void QuestionIndexUp()
        {
            if (Quest <= playQuestions.Length)
            {
                Quest++;
            }
        }

        public Pergunta CurrentQuestion()
        {
            return playQuestions[Quest];
        }
    }
}