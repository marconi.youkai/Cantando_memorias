﻿using UnityEngine;
using UnityEngine.Assertions;

namespace SiriusGames
{
    [CreateAssetMenu(fileName = "Sound Question", menuName = "question/sound question")]
    public class Pergunta_som : Pergunta
    {
        public AudioClip sound;
        private AudioSource musicSrc;

        private void OnValidate()
        {
            Assert.AreNotEqual(sound,null);
        }
    }
}