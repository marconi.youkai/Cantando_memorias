﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageQuestionDisplay : MonoBehaviour
{
    public Pergunta_Imagem imgQuestion;

    public Image questionImage;

    public TextMeshProUGUI question;
    public TextMeshProUGUI correctAnswer;
    public TextMeshProUGUI answer1;
    public TextMeshProUGUI answer2;

   
    void Start()
    {
        questionImage.sprite = imgQuestion.image;
        questionImage.preserveAspect = true;

        question.text = imgQuestion.question;
        correctAnswer.text = imgQuestion.correct_answer;
        answer1.text = imgQuestion.option_1;
        answer2.text = imgQuestion.option_2;
    }
}