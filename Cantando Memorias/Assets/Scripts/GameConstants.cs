﻿using UnityEngine;

namespace SiriusGames
{
    public class GameConstants : MonoBehaviour
    {
        public static int a = 0;
        public static int b = 0;
        public static int c = 0;

        public static int CARD_TO_DECK_COUNT = 0;
        public static int QUESTION_MAX = 15;
        public static bool MUTE= false;

        private void Start()
        {
            MUTE = ApplicationModel.currentMute;
        }
        
    }
        public class ApplicationModel
        {
            static public bool currentMute = false;
        }
}
