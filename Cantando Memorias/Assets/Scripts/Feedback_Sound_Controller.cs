﻿using UnityEngine;

namespace SiriusGames
{
    public class Feedback_Sound_Controller : MonoBehaviour
    {
        public AudioSource audioS;
        private AudioClip jingleCorrect;
        private AudioClip jingleWrong;
        private AudioClip jingleStreak;

        private void Start()
        {
            jingleCorrect = Resources.Load<AudioClip>("Media/Sound Effects/Acerto 03");
            jingleWrong = Resources.Load<AudioClip>("Media/Sound Effects/Erro 02");
            jingleStreak = Resources.Load<AudioClip>("Media/Sound Effects/Score Final (Acertou mais que 16)");
        }

        public void CorrectAnswer()
        {
            audioS.clip = jingleCorrect;
            StartAudio();
        }

        public void WrongAnswer()
        {
            audioS.clip = jingleWrong;
            StartAudio();
        }

        public void FinalScore()
        {
            audioS.clip = jingleStreak;
            StartAudio();
        }

        public void PlayAudio()
        {
            if (!audioS.isPlaying)
                StartAudio();
            else
                StopAudio();
        }

        public void StartAudio()
        {
            audioS.Play();
        }

        public void StopAudio()
        {
            audioS.Stop();
        }

        public void MuteToggle()
        {
            if (!GameConstants.MUTE)
            {
                audioS.volume = 1f;
            }
            else
            {
                audioS.volume = 0f;
            }
        }
    }
}