﻿using UnityEngine;

namespace SiriusGames
{
    public abstract class Pergunta : ScriptableObject
    {
        public string question;
        public string correct_answer;
        public string option_1;
        public string option_2;
        public Category category;

        public enum Category
        {
            InstrumentosMusicais,
            MusicasDeNovela,
            MusicasDeEpoca,
            Cantores,
            Cantigas,
            MusicasGospel,
        }

//        public void QuestionUpdate(SoundQuestionDisplay display)
//        {
//            display.question.text = this.question;
//            display.correctAnswer = this.correct_answer;
//            display.answer1 = this.option_1;
//            display.answer2 = this.option_2;
//
//            display.CreateAnswerDictionary();
//            // não remover o AnswerRandomizer() duplicado para ter melhor embaralhamento!!! 
//            display.AnswerRandomizer();
//            display.AnswerRandomizer();
//            display.AnswerTextAssignement();
//        }
    }
}