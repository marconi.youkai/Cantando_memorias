﻿using UnityEngine;

[CreateAssetMenu(fileName = "new question", menuName = "question/image question")]
public class Pergunta_Imagem : ScriptableObject
{
    public Sprite image;

    public string question;
    public string correct_answer;
    public string option_1;
    public string option_2;
    public Category category;

    public enum Category
    {
        instrumentos_musicais,
        Musicas_de_novela,
        Musicasde_de_epoca,
        Cantores,
        Cantigas,
        Musicas_Gospel,
    }
}