﻿using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SiriusGames
{
    public class UI_controller : MonoBehaviour
    {
        public GameObject screenStart;
        public GameObject screenQuestion;
        public GameObject screenAnswers;
        public GameObject screenLoading;
        public GameObject screenQuitGame;
        public GameObject screenQuitPartida;
        public GameObject screenCorrect;
        public GameObject screenWrong;
        public GameObject screenPontuação;
        public GameObject screenCredits;
        public GameObject tutorialPanel;
        public GameObject[] screensTutorial;
        public GameObject buttonA;
        public GameObject buttonB;
        public GameObject buttonC;
        public GameObject Alternativas;
        public GameObject Alternativa;
        public GameObject button_confirmar;
        public GameObject PontBoa;
        public GameObject PontRuim;
        public GameObject tic;
        public GameObject xis;
        public Button buttonNext;
        public GameObject[] muteButton = new GameObject[4];
        public GameObject[] mutedButton = new GameObject[4];
        public TextMeshProUGUI choosenAnswer;
        public TextMeshProUGUI correctAnswer;
        public TextMeshProUGUI correctAnswer2;
        public TextMeshProUGUI QuestionNumber;
        public TextMeshProUGUI Pontuação_Text;


        private SoundQuestionDisplay _soundQuestionDisplay;
        private Feedback_Sound_Controller _feedbackSound;
        private int activeTutScreen = 0;
        
//        public Pergunta _pergunta;
        private bool Loading;
        private float cont;
        private int Pont;

        public Game _game;

        private void Start()
        {
            _soundQuestionDisplay = GetComponent<SoundQuestionDisplay>();
            _feedbackSound = GetComponent<Feedback_Sound_Controller>();
            screenLoading.SetActive(true);
            screenStart.SetActive(false);
            screenQuestion.SetActive(false);
            screenAnswers.SetActive(false);
            screenPontuação.SetActive(false);
            Loading = true;
            cont = 4;
            Pont = 0;
            
        }

        public void StartGame()
        {
            _game.Quest = 0;
            _game.Shuffle();
            screenStart.SetActive(false);
            screenQuestion.SetActive(true);
            _soundQuestionDisplay.QuestionUpdate();
            //            CurrentQuestion().Update();
            QuestionNumberUpdater();
            ClearVisualFeedback();
            ActivateButtonNext(false);
            _soundQuestionDisplay.StartAudio();
        }

        public void Update()
        {
            if (Loading)
            {
                cont -= Time.deltaTime;
                if (cont <= 0)
                {
                    screenLoading.SetActive(false);
                    screenStart.SetActive(true);
                    tutorialPanel.SetActive(true);
                    Loading = false;
                }
            }
        }

        public void Back()
        {
            if (screenPontuação.activeInHierarchy)
            {
                screenCredits.SetActive(true);
            }

            if (screenAnswers.activeInHierarchy)
            {
                screenAnswers.SetActive(false);
            }
            else if (screenQuestion.activeInHierarchy)
            {
                screenQuestion.SetActive(false);
                screenStart.SetActive(true);
                _soundQuestionDisplay.StopAudio();
                PontBoa.SetActive(false);
                PontRuim.SetActive(false);
                screenPontuação.SetActive(false);
                Pont = 0;
            }
        }

        public void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
            #endif
        }

        public void ExitPartida()
        {
            screenStart.SetActive(true);
            screenQuestion.SetActive(false);
            screenAnswers.SetActive(false);
            screenLoading.SetActive(false);
            screenQuitGame.SetActive(false);
            screenQuitPartida.SetActive(false);
            screenPontuação.SetActive(false);
            PontBoa.SetActive(false);
            PontRuim.SetActive(false);
            Pont = 0;
            _soundQuestionDisplay.StopAudio();
        }

        public void QuitGame()
        {
            screenQuitGame.SetActive(true);
        }

        public void QuitPartida()
        {
            screenQuitPartida.SetActive(true);
        }

        public void BackQuitGame()
        {
            screenQuitGame.SetActive(false);
        }

        public void BackQuitPartida()
        {
            screenQuitPartida.SetActive(false);
        }


        public void ShowAnswers()
        {
            screenAnswers.SetActive(true);
        }

        public void AnswerSelected()
        {
            switch (EventSystem.current.currentSelectedGameObject.name)
            {
                case "botão_opção_A":
                    choosenAnswer.text = "A";
                    break;
                case "botão_opção_B":
                    choosenAnswer.text = "B";
                    break;
                case "botão_opção_C":
                    choosenAnswer.text = "C";
                    break;
            }

            displayCorrect();
            ClearVisualFeedback();
            Alternativas.SetActive(false);
            Alternativa.SetActive(true);
            ActivateButtonNext(true);
            screenAnswers.SetActive(false);
            button_confirmar.SetActive(true);
        }

        private void displayCorrect()
        {
            correctAnswer.text = _soundQuestionDisplay.CorrectAnswer();
            correctAnswer2.text = _soundQuestionDisplay.CorrectAnswer();
        }

        private void QuestionNumberUpdater()
        {
            if (_game.Quest < 9)
                QuestionNumber.text = "0" + (_game.Quest + 1);

            else
                QuestionNumber.text = "" + (_game.Quest + 1);
        }

        public void NextQUestion()
        {
            if (buttonNext.IsInteractable())
            {
                if (_game.Quest < GameConstants.QUESTION_MAX - 1)
                {
                    _game.QuestionIndexUp();
                    _soundQuestionDisplay.QuestionUpdate();
//                    _soundQuestionDisplay.StartAudio();
                    ClearAnswers();
                    ClearVisualFeedback();
                    QuestionNumberUpdater();
                    ActivateButtonNext();
                    Alternativas.SetActive(true);
                    Alternativa.SetActive(false);
                    button_confirmar.SetActive(false);
                }
                else
                {
                    Pontuação_Text.text = "" + Pont;

                    if (Pont <= 10)
                    {
                        Pontuação_Text.color = new Color32(141, 207, 244, 255);
                        PontRuim.SetActive(true);
                    }

                    else if (Pont > 10)
                    {
                        Pontuação_Text.color = new Color32(255, 200, 0, 255);
                        PontBoa.SetActive(true);
                    }

                    screenPontuação.SetActive(true);
                    _feedbackSound.FinalScore();
                }
            }

            else
            {
                buttonNext.interactable = false;
            }
        }

        private void ActivateButtonNext()
        {
            buttonNext.interactable = !buttonNext.IsInteractable();
        }

        private void ActivateButtonNext(bool b)
        {
            buttonNext.interactable = b;
        }

        private void ClearAnswers()
        {
            choosenAnswer.text = "";
            correctAnswer.text = "";
        }

        public void CheckAnswer()
        {
            _soundQuestionDisplay.StopAudio();
            if (choosenAnswer.text == correctAnswer.text)
            {
                screenCorrect.SetActive(true);
                _feedbackSound.CorrectAnswer();
                Pont += 1;
            }
            else
            {
                screenWrong.SetActive(true);
                _feedbackSound.WrongAnswer();
            }
        }

        public void MuteSound()
        {
            GameConstants.MUTE = !GameConstants.MUTE;

            if (!GameConstants.MUTE == false)
            {
                for (int i = 0; i < muteButton.Length; i++)
                {
                    muteButton[i].SetActive(false);
                }

                for (int i = 0; i < mutedButton.Length; i++)
                {
                    mutedButton[i].SetActive(true);
                }

                _feedbackSound.MuteToggle();
            }
            else
            {
                for (int i = 0; i < muteButton.Length; i++)
                {
                    muteButton[i].SetActive(true);
                }

                for (int i = 0; i < mutedButton.Length; i++)
                {
                    mutedButton[i].SetActive(false);
                }

                _feedbackSound.MuteToggle();
            }
        }

        public void StartTutorial()
        {
            tutorialPanel.SetActive(true);
            screensTutorial[0].SetActive(true);
            for (int i = 1; i < screensTutorial.Length; i++)
            {
                screensTutorial[i].SetActive(false);
            }
            screenStart.SetActive(false);
        }

        public void EndTutorial()
        {
            screenStart.SetActive(true);
            tutorialPanel.SetActive(false);
        }

        public void NextScreen()
        {
            screensTutorial[activeTutScreen].SetActive(false);
            activeTutScreen++;
            screensTutorial[activeTutScreen].SetActive(true);
        }

        public void LastScreen()
        {
            screensTutorial[activeTutScreen].SetActive(false);
            activeTutScreen--;
            screensTutorial[activeTutScreen].SetActive(true);
        }

        public void EndCredits()
        {
            SceneManager.LoadScene(0);
        }

        private void ClearVisualFeedback()
        {
            screenCorrect.SetActive(false);
            screenWrong.SetActive(false);
        }
    }
}