﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using Random = UnityEngine.Random;


namespace SiriusGames
{
    public class SoundQuestionDisplay : MonoBehaviour
    {
        public Game _game;

        private Pergunta QuestionContainer;

        public AudioSource musicSrc;
        public Button botaoSom1;
        public Button botaoSom2;

        public TextMeshProUGUI question;
        [HideInInspector] public string correctAnswer;
        [HideInInspector] public string answer1;
        [HideInInspector] public string answer2;
        public TextMeshProUGUI a;
        public TextMeshProUGUI b;
        public TextMeshProUGUI c;

        private Dictionary<string, Boolean> Answers = new Dictionary<string, Boolean>();

        public void QuestionUpdate()
        {
            QuestionContainer = _game.CurrentQuestion();

            _game = GameObject.Find("Game").GetComponent<Game>();


            if (QuestionContainer is Pergunta_som)
            {
                Pergunta_som soundQuestionContainer = (Pergunta_som) QuestionContainer;
                botaoSom1.interactable = true;
                botaoSom2.gameObject.SetActive(true);
                musicSrc.clip = soundQuestionContainer.sound;
                PlayAudio();
            }
            else
            {
                botaoSom1.interactable = false;
                botaoSom2.gameObject.SetActive(false);
                musicSrc.clip = null;
            }

            question.text = QuestionContainer.question;
            correctAnswer = QuestionContainer.correct_answer;
            answer1 = QuestionContainer.option_1;
            answer2 = QuestionContainer.option_2;

            CreateAnswerDictionary();
            // não remover o AnswerRandomizer() duplicado para ter melhor embaralhamento!!! 
            AnswerRandomizer();
            AnswerRandomizer();
            AnswerTextAssignement();
        }

        public void CreateAnswerDictionary()
        {
            Answers.Clear();
            Answers.Add(correctAnswer, true);
            Answers.Add(answer1, false);
            Answers.Add(answer2, false);
        }

        public void AnswerRandomizer()
        {
            //dictionary shuffle
            Answers = Answers.OrderBy(x => Random.Range(0, Answers.Count)).ToDictionary(item => item.Key, item => item.Value);
        }

        public string CorrectAnswer()
        {
            for (int i = 0; i < Answers.Count; i++)
            {
                if (Answers.ElementAt(i).Value)
                {
                    switch (i)
                    {
                        case 0:
                            return "A";
                            break;
                        case 1:
                            return "B";
                            break;
                        case 2:
                            return "C";
                            break;
                    }
                }
            }

            return "";
        }

        public void AnswerTextAssignement()
        {
            a.text = Answers.ElementAt(0).Key;
            b.text = Answers.ElementAt(1).Key;
            c.text = Answers.ElementAt(2).Key;
        }

        public void PlayAudio()
        {
            if (!musicSrc.isPlaying)
                StartAudio();
            else
                StopAudio();
        }

        public void StartAudio()
        {
            musicSrc.Play();
        }

        public void StopAudio()
        {
            musicSrc.Stop();
        }
    }
}